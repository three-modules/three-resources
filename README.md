# three-resources

A threejs module to manage resources: textures, materials, models, shaders, mathematic functions, ...

Includes some web helpers to visualize and organize resources.

This is a module for three+react and it doesn't intend to work on its own. 
Use it from the common parent project to see it work
