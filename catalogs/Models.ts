// Internal
// import astronaut from '../assets/models/Astronaut.glf';
// import horse from '../assets/models/Horse.glb';

import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import * as THREE from 'three'
import { FBXLoader } from "three/examples/jsm/loaders/FBXLoader";

// External
const avocado = "https://raw.githubusercontent.com/KhronosGroup/glTF-Sample-Models/master/2.0/Avocado/glTF/Avocado.gltf"
const lantern = "https://raw.githubusercontent.com/KhronosGroup/glTF-Sample-Models/master/2.0/Lantern/glTF/Lantern.gltf"
const sponza = "https://raw.githubusercontent.com/KhronosGroup/glTF-Sample-Models/master/2.0/Sponza/glTF/Sponza.gltf"
// const suzanne = "https://raw.githubusercontent.com/KhronosGroup/glTF-Sample-Models/master/2.0/Suzanne/glTF/Suzanne.gltf"
// https://threejs.org/examples/models/gltf/Xbot.glb
// https://threejs.org/examples/models/gltf/RobotExpressive/RobotExpressive.glb

// const astronaut = "/res";
export { /*astronaut, horse,*/ avocado, lantern, sponza };

const baseUrl =`${process.env.PUBLIC_URL}/assets/models/`

export class ModelsFactory {
    static instance: ModelsFactory
    static model

    static mixer
    static actions = {}
    static actionMapping = { walk: "Walk", run: "Run", idle: "Idle" }
    activeAction
    previousAction

    static gltfLoader = new GLTFLoader()
    static fbxLoader = new FBXLoader()

    model

    // static singleton = async (c: any) => {
    //     console.log(c)
    //     if (!c.instance) {
    //         console.log(`first call => create new ${c} instance`)
    //         c.instance = new c();
    //         await c.instance.init()
    //     }
    //     return c.model
    // }

    static create = async (className: any) => {
        if (!className.model) {
            console.log(`first call => load character class model`)
            await className.loadModel()
        }
        return className.model.clone()
    }


    // static async getModel() {
    //     // singleton
    //     if (!ModelsFactory.instance) {
    //         ModelsFactory.instance = new ModelsFactory();
    //         await ModelsFactory.instance.init()
    //     }
    //     return ModelsFactory.model;
    // }

    /**
     * initialize static model
     */
    static async loadModel(modelUrl, loader) {
        // loader should be the one specified in child class
        return new Promise(resolve => {
            loader.load(modelUrl, resolve,
                (xhr) => {
                    console.log((xhr.loaded / xhr.total) * 100 + '% loaded')
                },
                (error) => {
                    console.log(error)
                });
        });
    }

    async init() {
        console.log("init from base class")
    }
}

export class RobotExpressiveModel extends ModelsFactory {
    // static instance: RobotExpressiveModel
    // static model
    // static anims

    // static singleton = async () => ModelsFactory.singleton(RobotExpressiveModel)
    static actionMapping = { walk: "Walking", run: "Running", idle: "Idle" }

    constructor() {
        super()
        this.model = RobotExpressiveModel.model.clone()
    }

    /**
     * initialize static model
     */
    static async loadModel() {
        // await ModelsFactory.loadModel(`${baseUrl}RobotExpressive.glb`, ModelsFactory.gltfLoader)
        await super.loadModel(`${baseUrl}RobotExpressive.glb`, ModelsFactory.gltfLoader)
            .then((gltf: any) => {
                const model = gltf.scene;
                const animations = gltf.animations

                const mixer = new THREE.AnimationMixer(model);

                const actions = {};

                const states = ['Idle', 'Walking', 'Running', 'Dance', 'Death', 'Sitting', 'Standing'];
                const emotes = ['Jump', 'Yes', 'No', 'Wave', 'Punch', 'ThumbsUp'];

                for (let i = 0; i < animations.length; i++) {

                    const clip = animations[i];
                    const action = mixer.clipAction(clip);
                    actions[clip.name] = action;

                    if (emotes.indexOf(clip.name) >= 0 || states.indexOf(clip.name) >= 4) {

                        action.clampWhenFinished = true;
                        action.loop = THREE.LoopOnce;
                    }
                }

                RobotExpressiveModel.model = model
                RobotExpressiveModel.mixer = mixer
                RobotExpressiveModel.actions = actions
            });
    }
}

export class Xbot extends ModelsFactory {

    static actionMapping = { walk: "walk", run: "run", idle: "idle" }

    constructor() {
        super()
        this.model = Xbot.model.clone()
    }

    static async loadModel() {
        // const loader = new GLTFLoader();
        await super.loadModel(`${baseUrl}Xbot.glb`, Xbot.gltfLoader)
            .then((gltf: any) => {

                const model = gltf.scene;
                model.scale.setScalar(10)
                model.traverse(function (object: any) {

                    if (object.isMesh) object.castShadow = true;

                });

                // const skeleton = new THREE.SkeletonHelper( model );
                // skeleton.visible = false;
                // scene.add( skeleton );

                const animations = gltf.animations;
                const mixer = new THREE.AnimationMixer(model);

                const allActions = [];
                const baseActions = {
                    idle: { weight: 1 },
                    walk: { weight: 0 },
                    run: { weight: 0 }
                };
                const additiveActions = {
                    sneak_pose: { weight: 0 },
                    sad_pose: { weight: 0 },
                    agree: { weight: 0 },
                    headShake: { weight: 0 }
                };


                for (let i = 0; i !== animations.length; ++i) {

                    let clip = animations[i];
                    const name = clip.name;

                    if (baseActions[name]) {

                        const action = mixer.clipAction(clip);
                        // activateAction(action);
                        // baseActions[name].action = action;
                        baseActions[name] = action;
                        allActions.push(action);

                    } else if (additiveActions[name]) {

                        // Make the clip additive and remove the reference frame

                        THREE.AnimationUtils.makeClipAdditive(clip);

                        if (clip.name.endsWith('_pose')) {

                            clip = THREE.AnimationUtils.subclip(clip, clip.name, 2, 3, 30);

                        }

                        const action = mixer.clipAction(clip);
                        // activateAction(action);
                        additiveActions[name].action = action;
                        allActions.push(action);
                    }
                }

                Xbot.model = model
                Xbot.mixer = mixer
                Xbot.actions = baseActions
            });
    }
}