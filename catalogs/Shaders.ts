import * as THREE from "three";
import { Vector4, Uniform } from "three";
import * as TextureCatalog from "./Textures";

var Color = {

    uniforms: {
    },

    vertexShader: [
        "attribute vec3 color;",
        "varying vec3 col;",
        "void main()",
        "{",
        "   col = color;",
        "	vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );",
        "	gl_Position = projectionMatrix * mvPosition;",
        "}"
    ].join("\n"),

    fragmentShader: [
        "in vec3 col;",
        "void main( void ) {",
        "	gl_FragColor = vec4(col.r,col.g,col.b,1.0);",
        "}"
    ].join("\n")

};


var TriplanarTex = {

    uniforms: {
        "tex": { value: TextureCatalog.rock(1) }
    },

    vertexShader: [
        "varying vec3 pos;",
        "varying vec3 norm;",
        "void main()",
        "{",
        "   pos = position;",
        "   norm = normal;",
        "	vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );",
        "	gl_Position = projectionMatrix * mvPosition;",
        "}"
    ].join("\n"),

    fragmentShader: [
        "uniform sampler2D tex;",
        "in vec3 pos;",
        "in vec3 norm;",
        "vec4 triplanar_mapping( sampler2D tex, vec3 normal, vec3 position, float scale ) {",
        // Blending factor of triplanar mapping
        "	vec3 bf = normalize( abs( normal ) );",
        "	bf /= dot( bf, vec3( 1.0 ) );",
        // Triplanar mapping
        "	vec2 tx = position.yz * scale;",
        "	vec2 ty = position.zx * scale;",
        "	vec2 tz = position.xy * scale;",
        // Base color
        "	vec4 cx = texture2D(tex, tx) * bf.x;",
        "	vec4 cy = texture2D(tex, ty) * bf.y;",
        "	vec4 cz = texture2D(tex, tz) * bf.z;",
        "	return cx + cy + cz;",
        "}",
        "void main( void ) {",
        "	gl_FragColor = triplanar_mapping( tex, norm, pos, 0.01 );",
        "}"
    ].join("\n")

};


var TexColBlend = () => {
    return {
        uniforms: (color: THREE.Color) => {
            return {
                "tex": { value: TextureCatalog.rock(1) },
                "custCol": new Uniform(new Vector4(color.r, color.g, color.b, 0))
            }
        },

        vertexShader: [
            "uniform vec4 custCol;",
            "attribute vec3 color;",
            "varying vec4 col;",
            "varying vec3 pos;",
            "varying vec3 norm;",
            "void main()",
            "{",
            "   col = (custCol.r!=0.0 || custCol.g!=0.0 || custCol.b!=0.0)? custCol: vec4(color,custCol.a);",
            "   pos = position;",
            "   norm = normal;",
            "	vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );",
            "	gl_Position = projectionMatrix * mvPosition;",
            "}"
        ].join("\n"),

        fragmentShader: [
            "uniform sampler2D tex;",
            "in vec4 col;",
            "in vec3 pos;",
            "in vec3 norm;",
            "vec4 triplanar_mapping( sampler2D tex, vec3 normal, vec3 position, float scale ) {",
            // Blending factor of triplanar mapping
            "	vec3 bf = normalize( abs( normal ) );",
            "	bf /= dot( bf, vec3( 1.0 ) );",
            // Triplanar mapping
            "	vec2 tx = position.yz * scale;",
            "	vec2 ty = position.zx * scale;",
            "	vec2 tz = position.xy * scale;",
            // Base color
            "	vec4 cx = texture2D(tex, tx) * bf.x;",
            "	vec4 cy = texture2D(tex, ty) * bf.y;",
            "	vec4 cz = texture2D(tex, tz) * bf.z;",
            "	return (cx + cy + cz)*bf.x;",
            "}",
            "void main( void ) {",
            "vec4 triPlanTex = triplanar_mapping( tex, norm, pos, 0.01 );",
            "vec3 blend = triPlanTex.rgb*0.85 + col.rgb*0.15;",
            "	gl_FragColor = vec4(blend,col.a);",
            "}"
        ].join("\n")

    }
};

export { Color, TriplanarTex, TexColBlend };
